require 'rake/testtask'
Dir.glob('./{config,models,services,controllers,lib}/init.rb').each do |file|
  require file
end

task :default => [:spec]

namespace :db do
  require 'sequel'
  Sequel.extension :migration

  desc 'Run migrations'
  task :migrate do
    puts "Environment: #{ENV['RACK_ENV'] || 'development'}"
    puts 'Migrating to latest'
    Sequel::Migrator.run(DB, 'db/migrations')
  end

  desc 'Perform migration reset (full rollback and migration)'
  task :reset do
    Sequel::Migrator.run(DB, 'db/migrations', target: 0)
    Sequel::Migrator.run(DB, 'db/migrations')
  end
  desc 'Populate the database with test values'
  task :seed do
    load './db/seeds/accounts.rb'
  end
end

desc 'Run all the tests'
Rake::TestTask.new(name=:spec) do |t|
  t.pattern = 'spec/*_spec.rb'
end

namespace :key do
  require 'rbnacl/libsodium'
  require 'base64'

  desc 'Create rbnacl key'
  task :generate do
    key = RbNaCl::Random.random_bytes(RbNaCl::SecretBox.key_bytes)
    puts "KEY: #{Base64.strict_encode64 key}"
  end
end

namespace :service_key do
  require 'http'

  desc 'Get service key'
  task :get do
    response = HTTP.post("http://#{ENV['SHIPYARD_HOST']}:#{ENV['SHIPYARD_PORT']}/auth/login",
                         :json => { :username => ENV['SHIPYARD_ACCOUNT'],
                                    :password => ENV['SHIPYARD_PASSWORD']})
    msg = JSON.load(response)
    token = msg["auth_token"]

    # get service key
    response = HTTP.headers('X-Access-Token' => "#{ENV['SHIPYARD_ACCOUNT']}:#{token}")
                   .post("http://#{ENV['SHIPYARD_HOST']}:#{ENV['SHIPYARD_PORT']}/api/servicekeys", :json => {})
    msg = JSON.load(response)
    service_key = msg["key"]

    # print out
    puts "service_key: #{service_key}"
  end
end
