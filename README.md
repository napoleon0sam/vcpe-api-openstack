# vCPE API Openstack
API to manage Openstack intergration

## Table of contents

- [vCPE API]
  - [Show Available Images]
  - [Create VM]
  - [Show Available VMs]
  - [Delete VM]
  - [Initiate Network]
- [Openstack API]
  - [Get Openstack Token]
  - [VM]
    - [Image]
    - [Instance]
  - [Environment Config]
    - [Flavor]
    - [Network]
      - [Network]
      - [Subnet]
      - [Router]
      - [Link]
    - [Keypairs]
    - [Floating IP]
    - [Security Rules]
      - [Projects and Group]
      - [Allowed Traffic]
- [Install](#install)
  - [Gem Dependency](#gem-dependency)
  - [Setup Environment and Keys](#setup-environment-and-key)
  - [Database](#database)
- [Execute](#execute)


## 

## Install

### Gem Dependency
Install this API by cloning the *relevant branch* and installing required gems
```
$ bundle install
```

### Setup Environment and Key

For the first-time execution, it's necessary to new a `config_env.rb` file in `config` folder. (refer to `config_env.rb.example`)

This is in
```ruby
config_env :development, :test do
  set 'DB_KEY', '[database encryption key in base64 url safe encoding]'
  set 'JWK256', '[JWT encryption key in base64 url safe encoding]'
  set 'SERVICE_KEY', '[shipyard server token]'
  set 'SHIPYARD_HOST', '[shipyard server address]'
  set 'SHIPYARD_PORT', '[shipyard server port]'
  set 'SHIPYARD_ACCOUNT', '[Account for shipyard service]'
  set 'SHIPYARD_PASSWORD', '[Password for shipyard service]'
end
```

#### DB_KEY and JWK256
For this two credentials, please run
```
$ rake key:generate
```
twice to get two different keys and copy the keys to `DB_KEY` and `JWK256` in `config_env.rb` respectively.

#### Shipyard and Service key
Please check where your shipyard server and fill in `SHIPYARD_HOST` and `SHIPYARD_PORT`. Also, please fill your account and password to `SHIPYARD_ACCOUNT` and `SHIPYARD_PASSWORD`.

Then, you can run
```
$ rake service_key:get
```
to get service key and copy the key for `SERVICE_KEY` in the same file as well.


## Execute
Run this API by using:

```
$ rackup
```

If you want to change your port
```
$ rackup -p <port>
```
