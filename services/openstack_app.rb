require 'http'
require 'net/http'
require 'uri'
require 'json'

# Returns all projects belonging to an account
class OpenstackAppRest
  def self.get_tokens()
	#get the token to access openstack api server
    uri = URI.parse("http://127.0.0.1:5000/v2.0/tokens")
    body = {"auth": {"tenantName": "admin", "passwordCredentials": {"username": "admin", "password": "nomoresecret"}}}
    http = Net::HTTP.new(uri.host, uri.port)
    request = Net::HTTP::Post.new(uri.request_uri)
    request["Content-Type"] = "application/json"
    request["Accept"] = "application/json"
    request["User-Agent"] = "python-novaclient"
    request.body = body.to_json
    response = http.request(request)
    response.to_json
    json = JSON.parse(response.body)
    token = json["access"]["token"]["id"]
    return token

   
  end

  def self.create_instance(username:, name:, id:)
	#create a VM with 512M RAM and 20G disk
    token = self.get_tokens()
    image_id = id
	network, keypair, flavor = self.check_parameters("test_network", "ssh_key", "custom_flavor")
	vm_name = rand(0..999999)
    uri = URI.parse("http://127.0.0.1:8774/v2.1/servers")
    body = {"server": {"name": "vm_#{vm_name}", "imageRef": image_id, "key_name": keypair, "flavorRef": flavor, "min_count": 1, "max_count": 1, "networks": [{"uuid": network}]}}
    http = Net::HTTP.new(uri.host, uri.port)
    request = Net::HTTP::Post.new(uri.request_uri)
    request["Content-Type"] = "application/json"
    request["Accept"] = "application/json"
    request["X-Auth-Token"] = token
    request.body = body.to_json
    response = http.request(request)
    json = JSON.parse(response.body)
	vm_id = json["server"]["id"]
	ns = 7
	puts("sleep #{ns} before associate a floating IP")
	sleep ns
	self.associate_f_ip(vm_id)

  end

  def self.get_images(username)
	#show available images
    uri = URI.parse("http://127.0.0.1:9292/v2/images")
    http = Net::HTTP.new(uri.host, uri.port)
    request = Net::HTTP::Get.new(uri.request_uri)
    token = self.get_tokens()
    request["X-Auth-Token"] = token
    response = http.request(request)
    json = JSON.parse(response.body)
    response = http.request(request)
    response = response.body.to_json

  end

  def self.get_server(name, id)
	#show the details about a VM
	s = "http://127.0.0.1:8774/v2.1/servers/" + id
    uri = URI.parse(s)
    http = Net::HTTP.new(uri.host, uri.port)
    request = Net::HTTP::Get.new(uri.request_uri)
    token = self.get_tokens()
    request["X-Auth-Token"] = token
    response = http.request(request)
    json = JSON.parse(response.body)
	return json

  end

  def self.get_servers(username)
	#show available VMs
    uri = URI.parse("http://127.0.0.1:8774/v2.1/servers/detail")
    http = Net::HTTP.new(uri.host, uri.port)
    request = Net::HTTP::Get.new(uri.request_uri)
    token = self.get_tokens()
    request["X-Auth-Token"] = token
    response = http.request(request)
    json = JSON.parse(response.body)
    response = response.body.to_json

  end

  def self.del_servers(username:, name:, id:)
	#delete a selected VM
	json = self.get_server(name, id)
	f_ip = json["server"]["addresses"]["test_network"][1]["addr"]
	self.del_floating_ip(f_ip)	
    token = self.get_tokens()
    server_id = id
    uri = URI.parse("http://127.0.0.1:8774/v2.1/servers" + "/" +server_id)
    http = Net::HTTP.new(uri.host, uri.port)
    request = Net::HTTP::Delete.new(uri.request_uri)
    request["Content-Type"] = "application/json"
    request["Accept"] = "application/json"
    request["X-Auth-Token"] = token
    response = http.request(request)

  end

  def self.get_flavors(name)
	#show available flavers(for example, 512M RAM & 20G disk)
    uri = URI.parse("http://127.0.0.1:8774/v2.1/flavors/detail")
    http = Net::HTTP.new(uri.host, uri.port)
    request = Net::HTTP::Get.new(uri.request_uri)
    token = self.get_tokens()
    request["Content-Type"] = "application/json"
    request["Accept"] = "application/json"
    request["X-Auth-Token"] = token
    response = http.request(request)
    json = JSON.parse(response.body)
	found = false
	i = 0
    while i < json["flavors"].count
		iter_name = json["flavors"][i]["name"]
		if iter_name == name
			id = json["flavors"][i]["id"]
			found = true
			break
		end
        i = i + 1
    end
	return [found, id]

  end

  def self.create_flavor(name, id, ram, disk, vcpus)
	#create a flaver(for example, 512M RAM & 20G disk)
	flavor = self.get_flavors(name)
	if flavor[0] == false
		puts("flavor #{name} does not exist, create one")
	else
		puts("flavor #{name} exists, no need to create it")
		return -1
	end

	uri = URI.parse("http://127.0.0.1:8774/v2.1/flavors")
    body = {"flavor": {"vcpus": vcpus, "disk": disk, "name": name, "os-flavor-access:is_public": true, "rxtx_factor": 1.0, "OS-FLV-EXT-DATA:ephemeral": 0, "ram": ram, "id": id, "swap": 0}}
    http = Net::HTTP.new(uri.host, uri.port)
    request = Net::HTTP::Post.new(uri.request_uri)
    token = self.get_tokens()
    request["Content-Type"] = "application/json"
    request["Accept"] = "application/json"
    request["X-Auth-Token"] = token
    request.body = body.to_json
    response = http.request(request)
    json = JSON.parse(response.body)

  end


  def self.get_networks(name)
    #show available network
	uri = URI.parse("http://127.0.0.1:9696/v2.0/networks")
    http = Net::HTTP.new(uri.host, uri.port)
    request = Net::HTTP::Get.new(uri.request_uri)
    token = self.get_tokens()
    request["X-Auth-Token"] = token
    response = http.request(request)
    json = JSON.parse(response.body)
    i = 0
	found = false
    while i < json["networks"].count
		iter_name = json["networks"][i]["name"]
		if iter_name == name
			id = json["networks"][i]["id"]
			found = true
			break
		end
        i = i + 1
    end
	return [found, id]

  end

  def self.create_network(name)
	#create a network
	network = self.get_networks(name)
	if network[0] == true
		puts("network #{name} exists, don't need to create again")
		return
	else
		puts("network #{name} does not exists, try to create it")
	end
	network_id = network[1]
    token = self.get_tokens()
    uri = URI.parse("http://127.0.0.1:9696/v2.0/networks")
    body = {"network": {"name": name, "admin_state_up": true}}
    http = Net::HTTP.new(uri.host, uri.port)
    request = Net::HTTP::Post.new(uri.request_uri)
    request["Content-Type"] = "application/json"
    request["Accept"] = "application/json"
    request["X-Auth-Token"] = token
    request.body = body.to_json
    response = http.request(request)
    json = JSON.parse(response.body)

  end

  def self.get_subnet(name)
	#check if a default subnet exists
    uri = URI.parse("http://127.0.0.1:9696/v2.0/subnets")
    http = Net::HTTP.new(uri.host, uri.port)
    request = Net::HTTP::Get.new(uri.request_uri)
    token = self.get_tokens()
    request["X-Auth-Token"] = token
    response = http.request(request)
    json = JSON.parse(response.body)
    i = 0
	found = false
    while i < json["subnets"].count
		iter_name = json["subnets"][i]["name"]
		if iter_name == name
			id = json["subnets"][i]["id"]
			found = true
			break
		end
        i = i + 1
    end
	return [found, id]

  end

  def self.create_subnet(subnet_name, network_name, range)
	#create a subnet if the default one doesn't exist
	subnet = self.get_subnet(subnet_name)
	if subnet[0] == true
		puts("subnet #{subnet_name} exists, don't need to create again")
		return
	else
		puts("subnet #{subnet_name} does not exists, try to create it")
	end

	network = self.get_networks(network_name)
	if network[0] == true
		puts("found network #{network_name}, try to create a subnet")
	else
		puts("network #{network_name} does not exists, cannot create a subnet")
		return
	end
	network_id = network[1]
    token = self.get_tokens()
    uri = URI.parse("http://127.0.0.1:9696/v2.0/subnets")
    body = {"subnet": {"network_id": network_id, "ip_version": 4, "cidr": range, "dns_nameservers": ["8.8.8.8"], "name": subnet_name}}
    http = Net::HTTP.new(uri.host, uri.port)
    request = Net::HTTP::Post.new(uri.request_uri)
    request["Content-Type"] = "application/json"
    request["Accept"] = "application/json"
    request["X-Auth-Token"] = token
    request.body = body.to_json
    response = http.request(request)
    json = JSON.parse(response.body)

  end

  def self.get_router(name)
    #check if the default routers exists
	uri = URI.parse("http://127.0.0.1:9696/v2.0/routers")
    http = Net::HTTP.new(uri.host, uri.port)
    request = Net::HTTP::Get.new(uri.request_uri)
    token = self.get_tokens()
    request["X-Auth-Token"] = token
    response = http.request(request)
    json = JSON.parse(response.body)
    i = 0
	found = false
    while i < json["routers"].count
		iter_name = json["routers"][i]["name"]
		if iter_name == name
			id = json["routers"][i]["id"]
			found = true
			break
		end
        i = i + 1
    end
	return [found, id]

  end

  def self.create_router(name)
	#create a router if the default one doesn't exist
	router = self.get_router(name)
	if router[0] == true
		return
	end
	router_id = router[1]
    token = self.get_tokens()
    uri = URI.parse("http://127.0.0.1:9696/v2.0/routers")
	range = "192.168.0.0/24"
    body = {"router": {"name": name, "admin_state_up": true}}
    http = Net::HTTP.new(uri.host, uri.port)
    request = Net::HTTP::Post.new(uri.request_uri)
    request["Content-Type"] = "application/json"
    request["Accept"] = "application/json"
    request["X-Auth-Token"] = token
    request.body = body.to_json
    response = http.request(request)
    json = JSON.parse(response.body)

  end

  def self.link_router_external(router_name, network_name)
	#link a router to a network
	router = self.get_router(router_name)
	if router[0] == false
		puts("router #{router_name} does not exists, the link is not possible")
		return
	end
	router_id = router[1]

	network = self.get_networks(network_name)
	if network[0] == false
		puts("network #{network_name} does not exists, the link is not possible")
		return
	end
	network_id = network[1]

    token = self.get_tokens()
    uri = URI.parse("http://127.0.0.1:9696/v2.0/routers/" + router_id)
    body = {"router": {"external_gateway_info": {"network_id": network_id}}}
    http = Net::HTTP.new(uri.host, uri.port)
    request = Net::HTTP::Put.new(uri.request_uri)
    request["Content-Type"] = "application/json"
    request["Accept"] = "application/json"
    request["X-Auth-Token"] = token
    request.body = body.to_json
    response = http.request(request)
    json = JSON.parse(response.body)

  end

  def self.link_router_subnet(router_name, subnet_name)
	#link a router to a subnet
	router = self.get_router(router_name)
	if router[0] == false
		puts("router #{router_name} does not exists, link is not possible")
		return
	end
	router_id = router[1]

	subnet = self.get_subnet(subnet_name)
	if subnet[0] == false
		puts("subnet #{subnet_name} does not exists, link is not possible")
		return
	end
	subnet_id = subnet[1]

    token = self.get_tokens()
    uri = URI.parse("http://127.0.0.1:9696/v2.0/routers/" + router_id + "/add_router_interface")
    body = {"subnet_id": subnet_id}
    http = Net::HTTP.new(uri.host, uri.port)
    request = Net::HTTP::Put.new(uri.request_uri)
    request["Content-Type"] = "application/json"
    request["Accept"] = "application/json"
    request["X-Auth-Token"] = token
    request.body = body.to_json
    response = http.request(request)
    json = JSON.parse(response.body)

  end



  def self.get_keypairs(name)
	#check if a default keypair exists
    uri = URI.parse("http://127.0.0.1:8774/v2.1/os-keypairs")
    http = Net::HTTP.new(uri.host, uri.port)
    request = Net::HTTP::Get.new(uri.request_uri)
    token = self.get_tokens()
    request["X-Auth-Token"] = token
    response = http.request(request)
    json = JSON.parse(response.body)
    i = 0
	found = false
    while i < json["keypairs"].count
		iter_name = json["keypairs"][i]["keypair"]["name"]
		if iter_name == name
			keypair = iter_name
			found = true
			break
		end
        i = i + 1
    end
	return [found, keypair]

  end

  def self.check_parameters(network_name, keypair_name, flavor_name)
	#check if the environment is ready to create a VM
	network = self.get_networks(network_name)
	if network[0] == false
		puts("#{network_name} does not exist, u have to create one")
		return -1
	end
	keypair = self.get_keypairs(keypair_name)
	if keypair[0] == false
		puts("#{keypair_name} does not exist, u have to create one")
		return -1
	end
	flavor = self.get_flavors(flavor_name)
	if flavor[0] == false
		puts("#{flavor_name} does not exist, u have to create one")
		return -1
	end
	return [network[1], keypair[1], flavor[1]]
  end

  def self.create_floating_ip(network)
    uri = URI.parse("http://127.0.0.1:9696/v2.0/floatingips")
	id = self.get_networks(network)
	if id[0] == false
		puts("#{network} does not exist, u have to create it first")
		return -1
	end
    body = {"floatingip": {"floating_network_id": id[1]}}
    http = Net::HTTP.new(uri.host, uri.port)
    request = Net::HTTP::Post.new(uri.request_uri)
    token = self.get_tokens()
    request["X-Auth-Token"] = token
    request.body = body.to_json
    response = http.request(request)
    json = JSON.parse(response.body)
	f_network = json["floatingip"]["floating_network_id"]
	f_ip = json["floatingip"]["floating_ip_address"]
	return f_ip

  end

  def self.associate_f_ip(vm_id)
	#associate a floating IP for a VM
	f_ip = self.create_floating_ip("public")
    token = self.get_tokens()
	s = "http://127.0.0.1:8774/v2.1/servers/" + vm_id + "/action"
    uri = URI.parse(s)
    body = {"addFloatingIp": {"address": f_ip}}
    http = Net::HTTP.new(uri.host, uri.port)
    request = Net::HTTP::Post.new(uri.request_uri)
    request["Content-Type"] = "application/json"
    request["Accept"] = "application/json"
    request["X-Auth-Token"] = token
    request.body = body.to_json
    response = http.request(request)

  end

  def self.get_floating_ip(addr)
    #get the index of floating IP
	uri = URI.parse("http://127.0.0.1:9696/v2.0/floatingips")
    http = Net::HTTP.new(uri.host, uri.port)
    request = Net::HTTP::Get.new(uri.request_uri)
    token = self.get_tokens()
    request["X-Auth-Token"] = token
    response = http.request(request)
    json = JSON.parse(response.body)
    i = 0
    while i < json["floatingips"].count
		iter_addr = json["floatingips"][i]["floating_ip_address"]
		if iter_addr == addr
			id = json["floatingips"][i]["id"]
			break
		end
        i = i + 1
    end
	return id
	
  end


  def self.del_floating_ip(addr)
    #delete a floating IP
	f_ip_id = self.get_floating_ip(addr)
	token = self.get_tokens()
    uri = URI.parse("http://127.0.0.1:9696/v2.0/floatingips" + "/" + f_ip_id)
    http = Net::HTTP.new(uri.host, uri.port)
    request = Net::HTTP::Delete.new(uri.request_uri)
    request["Content-Type"] = "application/json"
    request["Accept"] = "application/json"
    request["X-Auth-Token"] = token
    response = http.request(request)

  end

  def self.get_project(name)
	#get the project, for example, admin
    uri = URI.parse("http://127.0.0.1/identity_admin/v3/projects")
    http = Net::HTTP.new(uri.host, uri.port)
    request = Net::HTTP::Get.new(uri.request_uri)
    token = self.get_tokens()
    request["X-Auth-Token"] = token
    response = http.request(request)
    json = JSON.parse(response.body)
	found = false
	i = 0
    while i < json["projects"].count
		iter_name = json["projects"][i]["name"]
		if iter_name == name
			id = json["projects"][i]["id"]
			found = true
			break
		end
        i = i + 1
    end
	return [found, id]

  end

  def self.get_group()
	#get the group, for example, admin-default
	project_id = self.get_project("admin")
	if project_id[0] == false
		puts("project admin does not exist")
		return -1
	end

    uri = URI.parse("http://127.0.0.1:9696/v2.0/security-groups")
    http = Net::HTTP.new(uri.host, uri.port)
    request = Net::HTTP::Get.new(uri.request_uri)
    token = self.get_tokens()
    request["X-Auth-Token"] = token
    response = http.request(request)
    json = JSON.parse(response.body)
    i = 0
	found = false
    while i < json["security_groups"].count
		iter_id = json["security_groups"][i]["project_id"]
		if iter_id == project_id[1]
			id = json["security_groups"][i]["id"]
			found = true
			break
		end
        i = i + 1
    end
	return [found, id]

  end

  def self.set_icmp_rule()
	#allow icmp traffic
	group = self.get_group()
	if group[0] == false
		puts("admin-default does not exist")
		return -1
	end

	uri = URI.parse("http://127.0.0.1:9696/v2.0/security-group-rules")
    body = {"security_group_rule": {"ethertype": "IPv4", "direction": "ingress", "remote_ip_prefix": "0.0.0.0/0", "protocol": "icmp", "security_group_id": group[1]}}
    http = Net::HTTP.new(uri.host, uri.port)
    request = Net::HTTP::Post.new(uri.request_uri)
    token = self.get_tokens()
    request["Content-Type"] = "application/json"
    request["Accept"] = "application/json"
    request["X-Auth-Token"] = token
    request.body = body.to_json
    response = http.request(request)
    json = JSON.parse(response.body)

  end

  def self.set_port_rule(port)
	#allow traffic for a specific port
	group = self.get_group()
	if group[0] == false
		puts("admin-default does not exist")
		return -1
	end

	uri = URI.parse("http://127.0.0.1:9696/v2.0/security-group-rules")
    body = {"security_group_rule": {"ethertype": "IPv4", "direction": "ingress", "remote_ip_prefix": "0.0.0.0/0", "protocol": "tcp", "port_range_max": port, "port_range_min": port, "security_group_id": group[1]}}
    http = Net::HTTP.new(uri.host, uri.port)
    request = Net::HTTP::Post.new(uri.request_uri)
    token = self.get_tokens()
    request["Content-Type"] = "application/json"
    request["Accept"] = "application/json"
    request["X-Auth-Token"] = token
    request.body = body.to_json
    response = http.request(request)
    json = JSON.parse(response.body)

  end
  
  def self.create_keypair(name, file) # X file
	#create a keypair
	uri = URI.parse("http://127.0.0.1:8774/v2.1/os-keypairs")
    body = {"keypair": {"name": name}}
    http = Net::HTTP.new(uri.host, uri.port)
    request = Net::HTTP::Post.new(uri.request_uri)
    token = self.get_tokens()
    request["Content-Type"] = "application/json"
    request["Accept"] = "application/json"
    request["X-Auth-Token"] = token
    request.body = body.to_json
    response = http.request(request)
    json = JSON.parse(response.body)

  end

  def self.import_keypair(name, file)
	#import a keypair

	uri = URI.parse("http://127.0.0.1:8774/v2.1/os-keypairs")
	contents = File.read(File.expand_path(file))
    body = {"keypair": {"public_key": contents, "name": name}}
    http = Net::HTTP.new(uri.host, uri.port)
    request = Net::HTTP::Post.new(uri.request_uri)
    token = self.get_tokens()
    request["Content-Type"] = "application/json"
    request["Accept"] = "application/json"
    request["X-Auth-Token"] = token
    request.body = body.to_json
    response = http.request(request)
    json = JSON.parse(response.body)

  end


  def self.initiate()
	#set the environment(must be done before creating a VM)
	self.create_network("test_network")
	self.create_subnet("test_subnet", "test_network", "192.168.0.0/24")
	self.create_router("test_router")
	self.link_router_external("test_router", "public")
	self.link_router_subnet("test_router", "test_subnet")
	self.set_icmp_rule()
	self.set_port_rule(22)
	self.set_port_rule(1999)
	self.create_flavor("custom_flavor", "00001", "512", "20", "1")
	self.import_keypair("ssh_key", "~/.ssh/id_rsa.pub")
	puts("initiation complete")

  end


end
