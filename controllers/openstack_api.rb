#require 'sinatra'

# Base class for ConfigShare Web Application
class VcpeAPI < Sinatra::Base
  post '/accounts/:username/openstack/launch' do
	#Send a request to create an instance
  	username = params[:username]
    image_setting = JSON.parse(request.body.read)
    response = OpenstackAppRest.create_instance(
	  username: username,
      name: image_setting['name'],
      id: image_setting['id'])
  end

  get '/accounts/:username/openstack/images' do
	#Send a request to see available images
	username = params[:username]
    response = OpenstackAppRest.get_images(username)
  end

  get '/accounts/:username/openstack/servers' do
	#Send a request to see available VMs
	username = params[:username]
    response = OpenstackAppRest.get_servers(username)
  end


  delete '/accounts/:username/openstack/servers' do
	#Send a request to delete a VM
	username = params[:username]
    vm_setting = JSON.parse(request.body.read)
    response = OpenstackAppRest.del_servers(
	  username: username,
      name: vm_setting['name'],
      id: vm_setting['id'])
  end


  get '/accounts/:username/openstack/initiate' do
	#Send a request to set VM network(must be done before creating a VM)
    response = OpenstackAppRest.initiate()
  end


end
