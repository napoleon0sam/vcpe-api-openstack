require 'sinatra'
require 'json'
require_relative 'config/environments'
require_relative 'models/init'

# Configuration Sharing Web Service
class VcpeAPI < Sinatra::Base
  before do
    host_url = "#{request.env['rack.url_scheme']}://#{request.env['HTTP_HOST']}"
    @request_url = URI.join(host_url, request.path.to_s)
  end

  get '/?' do
    'vCPE API service is up and running at /api/v1'
  end

  get '/api/v1/?' do
    # TODO: show all routes as json with links
  end

  get '/api/v1/tasks/?' do
    content_type 'application/json'

    JSON.pretty_generate(data: Task.all)
  end

  get '/api/v1/tasks/:id' do
    content_type 'application/json'

    id = params[:id]
    task = Task[id]

    if task
      JSON.pretty_generate(data: task, relationships: configurations)
    else
      halt 404, "PROJECT NOT FOUND: #{id}"
    end
  end

  post '/api/v1/tasks/?' do
    begin
      new_data = JSON.parse(request.body.read)
      Task.create(new_data)
    rescue => e
      logger.info "FAILED to create new task: #{e.inspect}"
      halt 400
    end

    status 201
  end
end
